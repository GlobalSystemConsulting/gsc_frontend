import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ModuloService {

  httpOptions: any
  options: any

  constructor(private http: HttpClient) { 
  }
  
  insertar (urlRest, data) {
    var wrapper = JSON.stringify(data)
    return this.http.post(environment.urls.BASE + urlRest, wrapper, {
      headers: {'autorizacion': localStorage.getItem('jwt')}
    })
  }

  listar (urlRest, data) {
    var wrapper = JSON.stringify(data)
    return this.http.get(environment.urls.BASE + urlRest,{
      headers: {'autorizacion': localStorage.getItem('jwt')},
      params: new HttpParams().set('content', wrapper),
    })
  }

  actualizar (urlRest, data) {
    var wrapper = JSON.stringify(data)
    this.httpOptions = {
      headers: {'autorizacion': localStorage.getItem('jwt'),
      'Content-Type': 'text/plain'
      }
    }
    return this.http.put(environment.urls.BASE + urlRest, wrapper, this.httpOptions)
  }


  eliminar (urlRest, data) {
    var wrapper = JSON.stringify(data)
    this.httpOptions = {
      headers: {'autorizacion': localStorage.getItem('jwt')},
      params: new HttpParams().set('content', wrapper),
    }
    return this.http.delete(environment.urls.BASE + urlRest, this.httpOptions)
  }  

  login (urlRest, data) {
    var wrapper = JSON.stringify(data)
    return this.http.post(environment.urls.BASE + urlRest, wrapper, this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'text/plain'
      })
    })
    
  }



}
