import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  // Estos no son los nombres de los campos
  KEY_REQUEST_STR:string 
  REQUEST_STR:string 
  REQUEST_ID_STR:string 
  REQUEST_IDENTITY_STR:string 
  REQUEST_SCOPE_STR:string 
  REQUEST_META_STR:string 
  REQUEST_DATA_STR:string 

  
  // Estos son los verdaderos nombres de los campos
  
  claves: any

  constructor() { 
    
    this.KEY_REQUEST_STR = "i.type";
    this.REQUEST_STR = "inet-req";
    this.REQUEST_ID_STR = "cmd";
    this.REQUEST_IDENTITY_STR = "identity";
    this.REQUEST_SCOPE_STR = "scope";
    this.REQUEST_META_STR = "meta";
    this.REQUEST_DATA_STR = "data";


    this.claves = {
      "i.type":"inet-req",
      "scope":"web",
      "identity":"dssd",
      "meta":{},
      "cmd":"login@1:signin",
      "data":{"nombre":"dssd","password":"ds"}
    }

  }
  
  getCmd() {
    return this.claves['cmd']
  }

  setCmd(dominio,version,accion){
    this.claves['cmd'] = dominio + "@" + version + ":" + accion
  }

  getIdentity () {
    return this.claves['identity']  
  }
    
  setIdentity(identity) {
    this.claves['identity'] = identity
  }

  getScope() {
    return this.claves['scope']
  }

  setScope(scope) {
    this.claves['scope'] = scope
  }

  getData() {
    return this.claves['data']
  }

  setData(data) {
    this.claves['data'] = data;
  }

  getMetadata() {
    //  return this[REQUEST_META_STR];
  }
}


  /*
  setMetadataValue (key,value){
      if(!(value instanceof Array))
          this.REQUEST_META_STR.[key] = [value];
  };

  this.setMetadataValues = function(key,values){
      if(values instanceof Array)
          this[REQUEST_META_STR][key] = values;
  };
  */

/*

 insertar: function( urlRest,data,sucess,error ){


/*
getCiudades () {
    return this.http.get(this.urlGlobal + this.ciudadURl, this.httpOptions = {
      headers: new HttpHeaders({
        'Authorization' : 'Bearer ' +sessionStorage.getItem('token')
      })
    })
}
*/