import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuInicioComponent } from '../../componente/menu-inicio/menu-inicio.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: MenuInicioComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),FormsModule, CommonModule],
  exports: [RouterModule],
  declarations: [MenuInicioComponent]
})
export class MenuInicioRoutingModule { }
