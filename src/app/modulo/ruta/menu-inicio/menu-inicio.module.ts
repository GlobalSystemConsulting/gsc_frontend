import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuInicioRoutingModule } from './menu-inicio-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MenuInicioRoutingModule
  ]
})
export class MenuInicioModule { }
