import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FuncionSistemaComponent } from '../../componente/funcion-sistema/funcion-sistema.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { CommonModule} from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: FuncionSistemaComponent
  }
]


@NgModule({
  imports: [RouterModule.forChild(routes),FormsModule, CommonModule],
  exports: [RouterModule],
  declarations: [FuncionSistemaComponent]
})
export class FuncionSistemaRoutingModule { }
