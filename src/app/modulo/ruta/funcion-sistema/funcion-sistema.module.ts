import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FuncionSistemaRoutingModule } from './funcion-sistema-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FuncionSistemaRoutingModule
  ],
  declarations: []
})
export class FuncionSistemaModule { }
