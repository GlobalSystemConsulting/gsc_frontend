import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DisenoCurricularComponent } from '../../../../componente/institucional/cuadro_horas/diseno-curricular/diseno-curricular.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { CommonModule} from '@angular/common';



const routes: Routes = [
  {
    path: '',
    component: DisenoCurricularComponent
  }
]


@NgModule({
  imports: [RouterModule.forChild(routes),FormsModule, CommonModule],
  exports: [RouterModule],
  declarations: [DisenoCurricularComponent]
})
export class DisenoCurricularRoutingModule { }
