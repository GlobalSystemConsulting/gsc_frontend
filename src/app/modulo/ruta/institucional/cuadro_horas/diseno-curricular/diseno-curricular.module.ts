import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DisenoCurricularRoutingModule } from './diseno-curricular-routing.module';

@NgModule({
  imports: [
    CommonModule,
    DisenoCurricularRoutingModule
  ],
  declarations: []
})
export class DisenoCurricularModule { }
