import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlertaSistemaRoutingModule } from './alerta-sistema-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AlertaSistemaRoutingModule
  ],
  declarations: []
})
export class AlertaSistemaModule { }
