import { NgModule } from '@angular/core';
import { CommonModule} from '@angular/common';

import { ModuloSistemaRoutingModule } from './modulo-sistema-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ModuloSistemaRoutingModule,
  ],
  declarations: []
})
export class ModuloSistemaModule { }
