import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuloSistemaComponent } from '../../componente/modulo-sistema/modulo-sistema.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { CommonModule} from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: ModuloSistemaComponent
  }
]


@NgModule({
  imports: [RouterModule.forChild(routes),FormsModule, CommonModule],
  exports: [RouterModule],
  declarations: [ModuloSistemaComponent]
})

export class ModuloSistemaRoutingModule { }
