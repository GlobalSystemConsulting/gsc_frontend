import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TipoOrganizacionRoutingModule } from './tipo-organizacion-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TipoOrganizacionRoutingModule
  ]
})
export class TipoOrganizacionModule { }
