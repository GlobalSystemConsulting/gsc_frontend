import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TipoOrganizacionComponent } from '../../componente/tipo-organizacion/tipo-organizacion.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: TipoOrganizacionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),FormsModule, CommonModule],
  exports: [RouterModule],
  declarations: [TipoOrganizacionComponent]
})
export class TipoOrganizacionRoutingModule { }
