import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RolesComponent } from '../../componente/roles/roles.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: RolesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),FormsModule, CommonModule],
  exports: [RouterModule],
  declarations: [RolesComponent]
})
export class RolesRoutingModule { }
