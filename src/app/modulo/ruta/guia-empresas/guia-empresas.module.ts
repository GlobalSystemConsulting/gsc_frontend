import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GuiaEmpresasRoutingModule } from './guia-empresas-routing.module';

@NgModule({
  imports: [
    CommonModule,
    GuiaEmpresasRoutingModule
  ],
  declarations: []
})
export class GuiaEmpresasModule { }
