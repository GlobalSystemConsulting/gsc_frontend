import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuiaEmpresasComponent } from '../../componente/guia-empresas/guia-empresas.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { CommonModule } from '@angular/common';
const routes: Routes = [
  {
    path: '',
    component: GuiaEmpresasComponent
  }
];



@NgModule({
  imports: [RouterModule.forChild(routes),FormsModule, CommonModule],
  exports: [RouterModule],
  declarations: [GuiaEmpresasComponent]
})
export class GuiaEmpresasRoutingModule { }
