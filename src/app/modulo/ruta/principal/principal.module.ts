import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrincipalRoutingModule } from './principal-routing.module';
@NgModule({
  imports: [
    CommonModule,
    PrincipalRoutingModule
  ],
  declarations: []
})
export class PrincipalModule { }
