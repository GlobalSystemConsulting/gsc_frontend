import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrincipalComponent } from '../../componente/principal/principal.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { TopMenuComponent } from '../../../modulo/componente/top-menu/top-menu.component';
import { LeftMenuComponent } from '../../../modulo/componente/left-menu/left-menu.component';
import { CommonModule } from '@angular/common';


const routes: Routes = [
  {
    path: '',
    component: PrincipalComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),FormsModule,CommonModule],
  exports: [RouterModule],
  declarations: [PrincipalComponent,TopMenuComponent,LeftMenuComponent
  ]
})
export class PrincipalRoutingModule { }
