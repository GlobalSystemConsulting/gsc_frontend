import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../../componente/login/login.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { CommonModule } from '@angular/common';
const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes),FormsModule, CommonModule],
  exports: [RouterModule],
  declarations: [LoginComponent]
})
export class LoginRoutingModule { }
