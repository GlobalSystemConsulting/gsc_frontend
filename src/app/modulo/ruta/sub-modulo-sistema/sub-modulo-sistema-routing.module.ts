import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubModuloSistemaComponent } from '../../componente/sub-modulo-sistema/sub-modulo-sistema.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: SubModuloSistemaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),FormsModule, CommonModule],
  exports: [RouterModule],
  declarations: [SubModuloSistemaComponent]
})
export class SubModuloSistemaRoutingModule { }
