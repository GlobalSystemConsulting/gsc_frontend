import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubModuloSistemaRoutingModule } from './sub-modulo-sistema-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SubModuloSistemaRoutingModule,
  ],
  declarations: []
})
export class SubModuloSistemaModule { }
