import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TipoAreaRoutingModule } from './tipo-area-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TipoAreaRoutingModule
  ]
})
export class TipoAreaModule { }
