import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TipoAreaComponent } from '../../componente/tipo-area/tipo-area.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: TipoAreaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),FormsModule, CommonModule],
  exports: [RouterModule],
  declarations: [TipoAreaComponent]
})
export class TipoAreaRoutingModule { }
