import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlertaSistemaRoutingModule } from './alerta-sistema-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AlertaSistemaRoutingModule
  ]
})
export class AlertaSistemaModule { }
