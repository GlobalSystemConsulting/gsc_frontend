import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlertaSistemaComponent } from '../../componente/alerta-sistema/alerta-sistema.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: AlertaSistemaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),FormsModule, CommonModule],
  exports: [RouterModule],
  declarations: [AlertaSistemaComponent]
})
export class AlertaSistemaRoutingModule { }
