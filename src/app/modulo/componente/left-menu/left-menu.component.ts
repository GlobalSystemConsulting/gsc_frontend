import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router' 

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.css']
})
export class LeftMenuComponent implements OnInit {

  menu: any;
  movil = true;
  vistaActual = [];

  menuAnteriorCompleto: any;
  usuMaster :any;
  menuPrincipal = [];
  funcionalidades = [];
  subModNom = "";
  funciones:any;
  modNom:any;
  visNom:any;

  constructor(private router: Router){
    this.usuMaster = {usuario:"",rol:"",organizacion:""};
    this.menuPrincipal = [];
   }
/*
  ngOnInit() {
    this.usuMaster.nombre = localStorage.getItem('nombre')
    this.usuMaster.rol = localStorage.getItem('rol')
    this.usuMaster.organizacion = JSON.parse(localStorage.getItem('organizacion'))
    var ayuda = JSON.parse(localStorage.getItem('modulos'))
    var indice = 0
    for (let a of ayuda) {
      a.indice = indice
      var nuevo1 = JSON.parse(JSON.stringify(a))
      var nuevo2 = JSON.parse(JSON.stringify(a))
      this.menu.push(nuevo1)
      this.menuAnteriorCompleto.push(nuevo2)
      indice ++
    }
    this.menu = JSON.parse( localStorage.getItem('menu'));
  }
*/
ngOnInit() {
  //configuracion inicial "no tocar" 
  this.usuMaster.usuario = JSON.parse( window.atob( localStorage.getItem('usuario')) );      
  this.usuMaster.rol = JSON.parse( window.atob(localStorage.getItem('rol')) );
  this.usuMaster.organizacion = JSON.parse( window.atob(localStorage.getItem('organizacion')) );
  
  
  this.menuPrincipal = JSON.parse( localStorage.getItem('modulos') );

        //sacando funcionalidades  //////////////////////////////////////////////////////////
        for(var i=0;i<this.menuPrincipal.length ;i++ ){
          var Modulost = this.menuPrincipal[i].subModulos;
          for(var j=0;j < Modulost.length; j++ ){                
              var funcionest = Modulost[j].funciones; 
              for(var k=0;k < funcionest.length;k++ ){ 
                  this.funcionalidades.push(funcionest[k]);                    
              }
          }  
      }
      console.log("Funcionalidades sacadas",this.funcionalidades)

      this.funciones = JSON.parse( localStorage.getItem('funciones') );
      this.menu = JSON.parse( localStorage.getItem('menu'));
      this.modNom = localStorage.getItem('modNom');
      this.subModNom = localStorage.getItem('subModNom');
      this.visNom = JSON.parse( localStorage.getItem('visNom'));
}

  menuToggle (e,subModulo){
    
    for (let s of this.menuAnteriorCompleto) {
      if (s.indice == subModulo.indice) {
        console.log('Entro xa:', subModulo)
        subModulo.subModulos = s.subModulos 
      }
    }
    
    for (let a of this.menu) {
      if (a.indice != subModulo.indice) {
        this.menu[a.indice].subModulos = []
        this.menu[a.indice].subModulos.push({})
      }
    }
    
  }

  elegirVista(vista) {
    var rutaNueva = 'principal/'+vista.clave;
    this.router.navigate([rutaNueva]);
  }

 /* elegirVista(vista,subModulo){
        
    this.vistaActual = vista;
    this.visNom = vista.nombre;
    localStorage.setItem('visNom', JSON.stringify(this.visNom));
    if(subModulo){
        this.subModNom = this.movil? subModulo.codigo: subModulo.nombre;;
        localStorage.setItem('subModNom', this.subModNom);
    }
   // this.router.navigate(['organizacion']);
    
};*/

}
