import { Component, OnInit } from '@angular/core'
import { ModuloService } from '../../../servicios/servicio'
import { RequestService } from '../../../servicios/request'
import { ToastrManager } from 'ng6-toastr-notifications'

@Component({
  selector: 'app-organizacion',
  templateUrl: './organizacion.component.html',
  styleUrls: ['./organizacion.component.css']
})
export class OrganizacionComponent implements OnInit {


  organizaciones: any
  tipoOrganizaciones: any
  nuevaOrganizacion: any = {
    codigo: String,
    nombre: String,
    alias: String,
    descripcion: String,
    direccion: String,
    estado: String,
    tipoOrganizacionID: Number,
    organizacionPadreID: Number,

    ubigeo: String,
    organizacionGestion: String,
    organizacionCaracteristica: String,
    organizacionPrograma: String,
    organizacionForma: String,
    organizacionVariante: String,
    organizacionModalidad: String
  }

  organizacionSel: any

  nuevoSelEditar: any = {
    codigo: String,
    nombre: String,
    alias: String,
    descripcion: String,
    direccion: String,
    estado: String,
    tipoOrganizacionID: String,
    organizacionPadreID: String,
    posicion: Number,
    organizacionID: Number,

    ubigeo: String,
    organizacionGestion: String,
    organizacionCaracteristica: String,
    organizacionPrograma: String,
    organizacionForma: String,
    organizacionVariante: String,
    organizacionModalidad: String
  }

  organizacionSeleccionado: any = {
    organizacionID: String,
    nombre: String,
    posicion: Number
  }



  constructor(private moduloServicio: ModuloService, private requestService: RequestService,
    public toastr: ToastrManager) {

    this.organizaciones = []
    this.tipoOrganizaciones = []

    this.nuevaOrganizacion.codigo = ''
    this.nuevaOrganizacion.nombre = ''
    this.nuevaOrganizacion.alias = ''
    this.nuevaOrganizacion.descripcion = ''
    this.nuevaOrganizacion.direccion = ''
    this.nuevaOrganizacion.estado = 'A'
    this.nuevaOrganizacion.tipoOrganizacionID = 0
    this.nuevaOrganizacion.organizacionPadreID = 0
    this.nuevaOrganizacion.ubigeo = ''
    this.nuevaOrganizacion.organizacionGestion = ''
    this.nuevaOrganizacion.organizacionCaracteristica = ''
    this.nuevaOrganizacion.organizacionPrograma = ''
    this.nuevaOrganizacion.organizacionForma = ''
    this.nuevaOrganizacion.organizacionVariante = ''
    this.nuevaOrganizacion.organizacionModalidad = ''

    this.nuevoSelEditar.codigo = ''
    this.nuevoSelEditar.nombre = ''
    this.nuevoSelEditar.alias = ''
    this.nuevoSelEditar.descripcion = ''
    this.nuevoSelEditar.direccion = ''
    this.nuevoSelEditar.estado = 'A'
    this.nuevoSelEditar.tipoOrganizacionID = '0'
    this.nuevoSelEditar.organizacionPadreID = '0'
    this.nuevoSelEditar.posicion = 0
    this.nuevoSelEditar.organizacionID = 0

    this.nuevoSelEditar.ubigeo = ''
    this.nuevoSelEditar.organizacionGestion = ''
    this.nuevoSelEditar.organizacionCaracteristica = ''
    this.nuevoSelEditar.organizacionPrograma = ''
    this.nuevoSelEditar.organizacionForma = ''
    this.nuevoSelEditar.organizacionVariante = ''
    this.nuevoSelEditar.organizacionModalidad = ''

    this.organizacionSel = {}

    this.organizacionSeleccionado.organizacionID = ''
    this.organizacionSeleccionado.nombre = ''
    this.organizacionSeleccionado.posicion = 0

  }

  ngOnInit() {
    this.listarOrganizaciones()
    this.listarTipoOrganizaciones()
  }



  listarOrganizaciones() {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('organizacion', 1, 'listarOrganizaciones')
    request.setData({})
    this.moduloServicio.listar('configuracionInicial', request.claves).subscribe(response => {
      this.organizaciones = Object(response).data
    }, error => {
      console.log('Este es el error: ', error)
    })
  }


  listarTipoOrganizaciones() {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('tipoOrganizacion', 1, 'listarTipoOrganizaciones')
    request.setData({})
    this.moduloServicio.listar('configuracionInicial', request.claves).subscribe(response => {
      this.tipoOrganizaciones = Object(response).data
    }, error => {
      console.log('Este es el error: ', error)
    })
  }


  agregarOrganizacion() {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('organizacion', 1, 'insertarOrganizacion')
    request.setData(this.nuevaOrganizacion)
    this.moduloServicio.insertar('configuracionInicial', request.claves).subscribe(response => {
      this.organizaciones.push(this.nuevaOrganizacion)
      console.log(this.nuevaOrganizacion)
      this.nuevaOrganizacion = {
        codigo: '',
        nombre: '',
        alias: '',
        descripcion: '',
        direccion: '',
        estado: 'A',
        tipoOrganizacionID: 0,
        organizacionPadreID: 0
      }
      this.toastr.successToastr(Object(response).responseMsg, 'Confirmacion')
    }, error => {
      console.log('Este es el error: ', error)
    })


  }

  eliminarOrganizacion (idDato, nombre, posicion) {
    this.organizacionSeleccionado.organizacionID = idDato
    this.organizacionSeleccionado.nombre = nombre
    this.organizacionSeleccionado.posicion = posicion
  }

  confirmacionElimanarModulo () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('organizacion',1,'eliminarOrganizacion')
    request.setData({organizacionID:this.organizacionSeleccionado.organizacionID})
    this.moduloServicio.eliminar("configuracionInicial", request.claves).subscribe(response => {
      this.organizaciones.splice(this.organizacionSeleccionado.posicion,1);
    }, error => {
      console.log('Este es el error: ', error)
    })
  }

  prepararEditar (m, indice) {
  
    this.nuevoSelEditar.codigo = m.codigo.trim()
    this.nuevoSelEditar.nombre = m.nombre.trim()
    this.nuevoSelEditar.alias = m.alias.trim()
    this.nuevoSelEditar.descripcion = m.descripcion.trim()
    this.nuevoSelEditar.direccion = m.direccion.trim()
    this.nuevoSelEditar.estado = m.estado
    this.nuevoSelEditar.tipoOrganizacionID = m.tipoOrganizacionID
    this.nuevoSelEditar.organizacionPadreID = m.organizacionPadreID
    this.nuevoSelEditar.posicion = indice
    this.nuevoSelEditar.organizacionID = m.organizacionID


    this.nuevoSelEditar.ubigeo = m.ubigeo
    this.nuevoSelEditar.organizacionGestion = m.organizacionGestion
    this.nuevoSelEditar.organizacionCaracteristica = m.organizacionCaracteristica
    this.nuevoSelEditar.organizacionPrograma = m.organizacionPrograma
    this.nuevoSelEditar.organizacionForma = m.organizacionForma
    this.nuevoSelEditar.organizacionVariante = m.organizacionVariante
    this.nuevoSelEditar.organizacionModalidad = m.organizacionModalidad

    console.log(this.nuevoSelEditar)
  }

  editarOrganizacion () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('organizacion',1,'actualizarOrganizacion')
    request.setData(this.nuevoSelEditar)
    this.moduloServicio.actualizar("configuracionInicial", request.claves).subscribe(response => {
      this.organizaciones[this.nuevoSelEditar.posicion] = this.nuevoSelEditar;
      this.nuevoSelEditar = {
        codigo: '',
        nombre: '',
        alias: '',
        descripcion: '',
        direccion: '',
        estado: '',
        tipoOrganizacionID: '',
        organizacionPadreID: '',
        posicion: 0,
        organizacionID: 0
      }
      this.toastr.successToastr(Object(response).responseMsg, 'Confirmacion')
    }, error => {
      console.log('Este es el error: ', error)
    })
  }

}
