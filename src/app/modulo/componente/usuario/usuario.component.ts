import { Component, OnInit } from '@angular/core';
import {ModuloService} from '../../../servicios/servicio'
import {RequestService} from '../../../servicios/request'
import { ToastrManager } from 'ng6-toastr-notifications'

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {


  usuarios: any
  roles: any
  organizaciones: any
  nuevoUsuario: any = {
    nombre:String,
    password:String,
    estado:String,
    rolID:Number,
    organizacionID:Number
  }
  usuarioSel: any

  nuevoSelEditar: any = {
    nombre:String,
    password:String,
    estado:String,
    rolID:Number,
    organizacionID:Number,
    posicion: Number,
    usuarioID: Number
  }
  settingUsuario: any;
    
  usuarioSeleccionado: any = {
    idMod: String,
    nombre: String,
    posicion: Number
  }
  
  constructor( private moduloServicio: ModuloService, private requestService: RequestService,
    public toastr: ToastrManager ) { 
    
    this.settingUsuario = {counts: []};

    this.usuarios = []
    this.roles = []
    this.organizaciones = []
    
    this.nuevoUsuario.nombre = ''
    this.nuevoUsuario.password = ''
    this.nuevoUsuario.estado = 'A'
    this.nuevoUsuario.rolID = 0,
    this.nuevoUsuario.organizacionID = 0

    this.nuevoSelEditar.nombre = ''
    this.nuevoSelEditar.password = ''
    this.nuevoSelEditar.estado = ''
    this.nuevoSelEditar.rolID = 0,
    this.nuevoSelEditar.organizacionID = 0
    this.nuevoSelEditar.posicion = 0
    this.nuevoSelEditar.usuarioID = 0

    this.usuarioSeleccionado.idMod = ''
    this.usuarioSeleccionado.nombre = ''
    this.usuarioSeleccionado.posicion = 0
    
    
    this.usuarioSel = {}


  }

  ngOnInit() {
    this.listarUsuarios()
    this.listarRoles()
    this.listarOrganizaciones()
  }

  listarUsuarios(){
    
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('usuarioSistema',1,'listarUsuarios')
    request.setData({})

    this.moduloServicio.listar('configuracionInicial', request.claves).subscribe(response => {
      this.settingUsuario = Object(response).data
    }, error => {
      console.log('Este es el error: ', error)
    })
  }

  agregarUsuario () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('usuarioSistema',1,'insertarUsuario')
    request.setData(this.nuevoUsuario)
    this.moduloServicio.insertar("configuracionInicial", request.claves).subscribe(response => {
      // this.usuarios.push(this.nuevoUsuario)
      this.listarUsuarios()
      this.nuevoUsuario = {
        nombre: '',
        password: '',
        estado: 'A',
        rolID: 0,
        organizacionID: 0
      }
      this.toastr.successToastr(Object(response).responseMsg, 'Confirmacion')
    }, error => {
      console.log('Este es el error: ', error)
    })
  }


  eliminarUsuario (idDato, nombre, posicion) {
    this.usuarioSeleccionado.idMod = idDato
    this.usuarioSeleccionado.nombre = nombre
    this.usuarioSeleccionado.posicion = posicion
  }

  confirmacionElimanarUsuario () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('usuarioSistema',1,'eliminarUsuario')
    request.setData({usuarioID:this.usuarioSeleccionado.idMod})
    this.moduloServicio.eliminar("configuracionInicial", request.claves).subscribe(response => {
      this.usuarios.splice(this.usuarioSeleccionado.posicion,1);
    }, error => {
      console.log('Este es el error: ', error)
    })
  }

    
  prepararEditar (m, indice) {
    this.nuevoSelEditar.nombre = m.nombre.trim()
    this.nuevoSelEditar.password = m.password.trim()
    this.nuevoSelEditar.estado = m.estado.trim()
    this.nuevoSelEditar.rolID = m.rolID
    this.nuevoSelEditar.organizacionID = m.organizacionID
    this.nuevoSelEditar.posicion = indice
    this.nuevoSelEditar.usuarioID = m.usuarioID
  }

  editarUsuario () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('usuarioSistema',1,'actualizarUsuario')
    request.setData(this.nuevoSelEditar)

    this.moduloServicio.actualizar("configuracionInicial", request.claves).subscribe(response => {
      this.usuarios[this.nuevoSelEditar.posicion] = this.nuevoSelEditar;
      this.nuevoSelEditar = {
        nombre:'',
        password:'',
        estado:'',
        rolID:0,
        organizacionID:0,
        posicion: 0,
        usuarioID: 0
      }
      this.toastr.successToastr(Object(response).responseMsg, 'Confirmacion')
    }, error => {
      console.log('Este es el error: ', error)
    })
  }



  listarRoles () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('rol',1,'listarRoles')
    request.setData({})
    this.moduloServicio.listar('configuracionInicial', request.claves).subscribe(response => {
      this.roles = Object(response).data
      console.log(this.usuarios)
    }, error => {
      console.log('Este es el error: ', error)
    })
  }
  
  listarOrganizaciones () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('organizacion',1,'listarOrganizaciones')
    request.setData({})
    this.moduloServicio.listar('configuracionInicial', request.claves).subscribe(response => {
      this.organizaciones = Object(response).data
      console.log(this.usuarios)
    }, error => {
      console.log('Este es el error: ', error)
    })
  } 

}
