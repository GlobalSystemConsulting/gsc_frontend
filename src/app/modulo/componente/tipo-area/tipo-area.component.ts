import { Component, OnInit } from '@angular/core';
import { ModuloService } from '../../../servicios/servicio'
import { RequestService } from '../../../servicios/request'
import { ToastrManager } from 'ng6-toastr-notifications'

@Component({
  selector: 'app-tipo-area',
  templateUrl: './tipo-area.component.html',
  styleUrls: ['./tipo-area.component.css']
})
export class TipoAreaComponent implements OnInit {

  tipoArea: any

  nuevoTipoArea: any = {
    nombre : String,
    alias : String,
    estado : String,
  }

  editTipoArea: any = {
    nombre : String,
    alias : String,
    estado : String,
  }
  tipoAreaSeleccionado: any = {
    organizacionID: String,
    nombre: String,
    posicion: Number
  }

  constructor(private moduloServicio: ModuloService, private requestService: RequestService,
    public toastr: ToastrManager) {

     this.tipoArea=[]

    this.nuevoTipoArea.nombre = ''
    this.nuevoTipoArea.alias = ''
    this.nuevoTipoArea.estado = ''

    this.editTipoArea.nombre = ''
    this.editTipoArea.alias = ''
    this.editTipoArea.estado = ''

    this.tipoAreaSeleccionado.organizacionID = ''
    this.tipoAreaSeleccionado.nombre = ''
    this.tipoAreaSeleccionado.posicion = 0
    }

    ngOnInit() {
      this.listarTipoAreas();
    }

    listarTipoAreas(){
      var request = new RequestService()
      request.setIdentity("admin")
      request.setScope('web')
      request.setCmd('area', 1, 'listarTipoAreas')
      request.setData({})

      this.moduloServicio.listar("configuracionInicial",request.claves).subscribe(response =>{
            this.tipoArea=Object(response).data
        },error =>{
            console.log('Este es el error',error);
        });
    }

  agregarTipoArea() {
      var request = new RequestService()
      request.setIdentity("admin")
      request.setScope('web')
      request.setCmd('area', 1, 'insertarTipoArea')
      request.setData(this.nuevoTipoArea)
      this.moduloServicio.insertar('configuracionInicial', request.claves).subscribe(response => {
        this.tipoArea.push(this.nuevoTipoArea)
        console.log(this.nuevoTipoArea)
        this.nuevoTipoArea = {
          nombre: '',
          alia: '',
          estado: 'A'
          
        }
        this.toastr.successToastr(Object(response).responseMsg, 'Confirmacion')
      }, error => {
        console.log('Este es el error: ', error)
      })


    }

    eliminarTipoArea (idDato, nombre, posicion) {
      this.tipoAreaSeleccionado.organizacionID = idDato
      this.tipoAreaSeleccionado.nombre = nombre
      this.tipoAreaSeleccionado.posicion = posicion
    }

    confirmacionElimanarModulo () {
      var request = new RequestService()
      request.setIdentity("admin")
      request.setScope('web')
      request.setCmd('area',1,'eliminarTipoArea')
      request.setData({tipoAreaID:this.tipoAreaSeleccionado.organizacionID})
      this.moduloServicio.eliminar("configuracionInicial", request.claves).subscribe(response => {
        this.tipoArea.splice(this.tipoAreaSeleccionado.posicion,1);
      }, error => {
        console.log('Este es el error: ', error)
      })
    }

    prepararEditar (m, indice) {
    
      this.editTipoArea.nombre = m.nombre.trim()
      this.editTipoArea.alias = m.alias.trim()
      this.editTipoArea.estado = m.estado
    

      console.log(this.editTipoArea)
    }

    editarTipoArea() {
      var request = new RequestService()
      request.setIdentity("admin")
      request.setScope('web')
      request.setCmd('area',1,'actualizarTipoArea')
      request.setData(this.editTipoArea)
      this.moduloServicio.actualizar("configuracionInicial", request.claves).subscribe(response => {
        this.tipoArea[this.editTipoArea.posicion] = this.editTipoArea;
        this.editTipoArea = {
          nombre: '',
          alias: '',
          estado: ''

        }
        this.toastr.successToastr(Object(response).responseMsg, 'Confirmacion')
      }, error => {
        console.log('Este es el error: ', error)
      })
    }

  }



