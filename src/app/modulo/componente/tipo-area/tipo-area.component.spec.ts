import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoAreaComponent } from './tipo-area.component';

describe('TipoAreaComponent', () => {
  let component: TipoAreaComponent;
  let fixture: ComponentFixture<TipoAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
