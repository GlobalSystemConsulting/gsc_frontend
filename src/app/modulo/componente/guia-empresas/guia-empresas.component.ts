import { Component, OnInit } from '@angular/core';
import { ModuloService } from '../../../servicios/servicio'
import { RequestService } from '../../../servicios/request'
import { ToastrManager } from 'ng6-toastr-notifications'

@Component({
  selector: 'app-guia-empresas',
  templateUrl: './guia-empresas.component.html',
  styleUrls: ['./guia-empresas.component.css']
})
export class GuiaEmpresasComponent implements OnInit {

  guiaEmpresas: any
  //tipoOrganizaciones: any
  nuevaEmpresa = {empresaID:"",razonSocial:"",ruc:"",estado:'A',telefono:"",pagWeb:"",existe:true};

  constructor(private moduloServicio: ModuloService, private requestService: RequestService,
    public toastr: ToastrManager) {

      this.guiaEmpresas = []

    }
    listarEmpresas(){

      var request = new RequestService()
      request.setIdentity("admin")
      request.setScope('web')
      request.setCmd('empresas', 1, 'listarEmpresas')
      request.setData({})

      this.moduloServicio.listar("configuracionInicial",request.claves).subscribe(response =>{
            this.guiaEmpresas=Object(response).data
        },error =>{
            console.log('Este es el error',error);
        });
    }

    
  ngOnInit() {
    this.listarEmpresas()
  }

}
