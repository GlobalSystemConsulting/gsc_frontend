import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuiaEmpresasComponent } from './guia-empresas.component';

describe('GuiaEmpresasComponent', () => {
  let component: GuiaEmpresasComponent;
  let fixture: ComponentFixture<GuiaEmpresasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuiaEmpresasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuiaEmpresasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
