import { Component, OnInit } from '@angular/core';
import { ModuloService } from '../../../servicios/servicio'
import { RequestService } from '../../../servicios/request'
import { ToastrManager } from 'ng6-toastr-notifications'


@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  roles: any
  
  subModulos: any
  subModulosSel: any
  
  subModulosEdi: any

  subModulosSelEdi: any

  nuevoRol: any = {
    abreviatura: String,
    nombre: String,
    descripcion: String,
    estado: String,
  }

  rolesSeleccionado: any = {
    tipoOrganizacionID: String,
    nombre: String,
    posicion: Number
  }

  rolSelec: Boolean
  
  
  constructor(private moduloServicio: ModuloService, private requestService: RequestService,
    public toastr: ToastrManager) {

      this.roles = []
      this.subModulos = []
      this.subModulosSel = []

      this.nuevoRol.abreviatura = ''
      this.nuevoRol.nombre = ''
      this.nuevoRol.descripcion = ''
      this.nuevoRol.estado = 'A'

      this.rolesSeleccionado.tipoOrganizacionID = ''
      this.rolesSeleccionado.nombre = ''
      this.rolesSeleccionado.posicion = 0

      this.rolSelec = false
    }

    ngOnInit() {
      this.listarRoles()
      this.listarSubModulos()
    }
  
    listarRoles(){

      var request = new RequestService()
      request.setIdentity("admin")
      request.setScope('web')
      request.setCmd('rol', 1, 'listarRolesConFunciones')
      request.setData({})
      this.moduloServicio.listar("configuracionInicial",request.claves).subscribe(response =>{
            this.roles=Object(response).data
        },error =>{
            console.log('Este es el error',error);
        });
    }
    listarSubModulos(){

      var request = new RequestService()
      request.setIdentity("admin")
      request.setScope('web')
      request.setCmd('subModuloSistema', 1, 'listarSubModulosConFunciones')
      request.setData({})
      this.moduloServicio.listar("configuracionInicial",request.claves).subscribe(response =>{
            this.subModulos=Object(response).data
        },error =>{
            console.log('Este es el error',error);
        });
    }


    eliminarRol (idDato, nombre, posicion) {
      this.rolesSeleccionado.tipoOrganizacionID = idDato
      this.rolesSeleccionado.nombre = nombre
      this.rolesSeleccionado.posicion = posicion
    }
    confirmacionElimanarModulo () {
      var request = new RequestService()
      request.setIdentity("admin")
      request.setScope('web')
      request.setCmd('tipoOrganizacion',1,'eliminarTipoOrganizacion')
      request.setData({rolID:this.rolesSeleccionado.tipoOrganizacionID})
      this.moduloServicio.eliminar("configuracionInicial", request.claves).subscribe(response => {
        this.roles.splice(this.rolesSeleccionado.posicion,1);
      }, error => {
        console.log('Este es el error: ', error)
      })
    }

    seleccionarFuncion(i,j){
      this.rolSelec=false
      var subMod = this.subModulos[i]
      var fun = subMod.funciones[j]
      
      fun.tipo = 3;
      fun.dependencias = "0"
      
      if( subMod.sel >= 0){
          var subModSel = this.subModulosSel[subMod.sel]
          subModSel.funciones.push(fun)
          fun.ver = true
          if(subMod.funciones.length==subModSel.funciones.length)
              subMod.ver = true
      }
      else{
          subMod.sel = this.subModulosSel.length
          var nuevoSubMod = {codigo:subMod.codigo,nombre:subMod.nombre,subModuloID:subMod.subModuloID,sel:i,funciones:[]}
          
          nuevoSubMod.funciones.push(fun)
          fun.ver = true
          this.subModulosSel.push(nuevoSubMod)
          
          if(subMod.funciones.length==nuevoSubMod.funciones.length)
              subMod.ver = true
      }

  }

  desSeleccionarFuncion(i,j){
        
    var subMod = this.subModulosSel[i];
    var fun = subMod.funciones[j];
    
    fun.ver = false;
    subMod.funciones.splice(j,1);
    
    if(subMod.funciones.length < this.subModulos[subMod.sel].funciones.length){
        this.subModulos[subMod.sel].ver = false;            
    }
    if(subMod.funciones.length===0){
        this.subModulosSel.splice(i,1);
        delete this.subModulos[subMod.sel].sel;
        this.rolSelec=true;
    }
  }

}
