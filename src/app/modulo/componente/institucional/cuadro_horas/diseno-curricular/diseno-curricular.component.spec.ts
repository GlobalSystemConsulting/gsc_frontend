import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisenoCurricularComponent } from './diseno-curricular.component';

describe('DisenoCurricularComponent', () => {
  let component: DisenoCurricularComponent;
  let fixture: ComponentFixture<DisenoCurricularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisenoCurricularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisenoCurricularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
