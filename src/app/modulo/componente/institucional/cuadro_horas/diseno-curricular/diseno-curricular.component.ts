import { Component, OnInit } from '@angular/core'
import { ModuloService } from '../../../../../servicios/servicio'
import { RequestService } from '../../../../../servicios/request'


@Component({
  selector: 'app-diseno-curricular',
  templateUrl: './diseno-curricular.component.html',
  styleUrls: ['./diseno-curricular.component.css']
})
export class DisenoCurricularComponent implements OnInit {


  params = { count: 1 }
  setting = { counts: [] }

  nuevoDiseno = { nombre: "", descripcion: "", resolucion: "", tipo: 'N', estado: 'A' }
  disenoSel = {}
  //tabla = new NgTableParams(params, setting)
  nivel = {}
  ciclo = {}
  jornada = {}
  jornadas = {}

  grado = {}
  area = { tipo: false }
  gradoArea = {}
  finFase1 = false
  matris: any
  matris2 = {}
  passwordToAuthenticate = ""
  usuarioValidado = false

  estaCreado: boolean

  dataGeneral: any
  organizacionID: String

  disenoActual: any


  constructor(private moduloServicio: ModuloService, private requestService: RequestService) {

    this.estaCreado = false
    this.jornadas = []
    this.dataGeneral = []
    this.organizacionID = ''

    this.disenoActual = {}

    this.matris = {}

  }

  ngOnInit() {
    this.organizacionID = JSON.parse(window.atob(localStorage.getItem("organizacion"))).organizacionID
    this.listarDisenos()


  }

  listarDisenos() {
    var orgID = JSON.parse(window.atob(localStorage.getItem("organizacion"))).organizacionID
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('diseñoCurricular', 1, 'listarDiseñoCurricular')
    request.setData({})
    this.moduloServicio.listar('cuadroHoras', request.claves).subscribe(response => {
      console.log('Esta es la respuesta: ', Object(response).data)
      this.dataGeneral = Object(response).data

      for (let a of this.dataGeneral) {
        this.disenoActual = a
      }
      console.log('Esta es la respuesta: ', this.disenoActual.modalidad.nombre)
      var objetoRes = Object(response).data
      //this.setting.dataset = objetoRes
      for (var i = 0; i < objetoRes.length; i++) {
        if (objetoRes[i].fecha.search((new Date()).getFullYear()) == 0 || objetoRes[i].organizacionID == orgID) {
          this.estaCreado = true
          break
        }
      }
      /*
      this.tabla.settings(setting)
      this.tabla.calcularMatriz()
      this.tabla.calcularMatrizDetalle()
      */
      request.setIdentity("admin")
      request.setScope('web')
      request.setCmd('diseñoCurricular', 1, 'listarJornadaEscolar')
      request.setData({})

      this.calcularMatriz()
      this.calcularMatrizDetalle()

      this.moduloServicio.listar('cuadroHoras', request.claves).subscribe(response => {
        var objetoRes = Object(response).data
        this.jornadas = objetoRes
      })
    }, error => {
      console.log('Este es el error: ', error)
    })
  }

  calcularMatriz() {
    var disenoActual = this.disenoActual
    if (disenoActual && disenoActual.grados) {
      var niveles = []
      var nAnt = { id: 0, c: 1, nom: "" }
      var ciclos = []
      var cAnt = { id: 0, c: 1, nom: "" }
      disenoActual.grados.forEach(function (item) {
        if (nAnt.id == item.nivelID)
          nAnt.c++
        else {
          nAnt = { id: item.nivelID, c: 1, nom: item.nivel }
          niveles.push(nAnt)
        }
        if (cAnt.id == item.cicloID)
          cAnt.c++
        else {
          cAnt = { id: item.cicloID, c: 1, nom: item.ciclo }
          ciclos.push(cAnt)
        }
      })
      this.matris.niveles = niveles
      this.matris.ciclos = ciclos
      this.matris.c = disenoActual.grados.length
    }
  }

  calcularMatrizDetalle() {
    this.matris.gradoAreas = []
    if (this.disenoActual){
      this.disenoActual.gradoAreas.forEach( (item) => {
        var areas = this.matris.gradoAreas[item.areaPos]
        if (!areas) {
          areas = this.matris.gradoAreas[item.areaPos] = []
        }
        areas[item.gradoPos] = JSON.parse(JSON.stringify(item))
        areas[item.gradoPos].nombre = this.disenoActual.areas[item.areaPos].nombre
      })
    }

    console.log('Esta es la matriz completa: ', this.matris.gradoAreas)
  }


}
