import { Alert } from 'selenium-webdriver';
import { Component, OnInit } from '@angular/core';
import {ModuloService} from '../../../servicios/servicio'
import {RequestService} from '../../../servicios/request'
import { ToastrManager } from 'ng6-toastr-notifications'

@Component({
  selector: 'app-modulo-sistema',
  templateUrl: './modulo-sistema.component.html',
  styleUrls: ['./modulo-sistema.component.css']
})

export class ModuloSistemaComponent implements OnInit {

  modulos: any
  
  nuevoModulo: any = {
    codigo: String,
    nombre: String,
    descripcion: String,
    icono: String,
    estado: String
  }
  
  nuevoSelEditar: any = {
    codigo: String,
    nombre: String,
    descripcion: String,
    icono: String,
    estado: String,
    posicion: Number,
    moduloID: Number
  }

  moduloSel: any
  
  moduloSeleccionado: any = {
    idMod: String,
    nombre: String,
    posicion: Number
  }


  constructor( private moduloServicio: ModuloService, private requestService: RequestService,
    public toastr: ToastrManager ) { 

    this.modulos = []
    this.nuevoModulo.codigo = ""
    this.nuevoModulo.nombre = ""
    this.nuevoModulo.descripcion = ""
    this.nuevoModulo.icono = ""
    this.nuevoModulo.estado = 'A'
   

    this.nuevoSelEditar.codigo = ""
    this.nuevoSelEditar.nombre = ""
    this.nuevoSelEditar.descripcion = ""
    this.nuevoSelEditar.icono = ""
    this.nuevoSelEditar.estado = ''
    this.nuevoSelEditar.posicion = 0
    this.nuevoSelEditar.moduloID = 0


    this.moduloSeleccionado.idMod = ''
    this.moduloSeleccionado.nombre = ''
    this.moduloSeleccionado.posicion = 0
    
  }

  ngOnInit() {
    this.listarModulos()
  }


  listarModulos() {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('moduloSistema',1,'listarModulos')
    request.setData({})
    this.moduloServicio.listar('configuracionInicial', request.claves).subscribe(response => {
      this.modulos = Object(response).data
    }, error => {
      console.log('Este es el error: ', error)
    })
  }

  eliminarModulo (idDato, nombre, posicion) {
    this.moduloSeleccionado.idMod = idDato
    this.moduloSeleccionado.nombre = nombre
    this.moduloSeleccionado.posicion = posicion
  }

  confirmacionElimanarModulo () {
    
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('moduloSistema',1,'eliminarModulo')
    request.setData({moduloID:this.moduloSeleccionado.idMod})
    this.moduloServicio.eliminar("configuracionInicial", request.claves).subscribe(response => {
      this.modulos.splice(this.moduloSeleccionado.posicion,1);
    }, error => {
      console.log('Este es el error: ', error)
    })
  }

  
  agregarModulo () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('moduloSistema',1,'insertarModulo')
    request.setData(this.nuevoModulo)
    this.moduloServicio.insertar("configuracionInicial", request.claves).subscribe(response => {
      this.modulos.push(this.nuevoModulo)
      this.nuevoModulo = {
        codigo: '',
        nombre: '',
        descripcion: '',
        icono: '',
        estado: 'A'
      }
      this.toastr.successToastr(Object(response).responseMsg, 'Confirmacion')
    }, error => {
      console.log('Este es el error: ', error)
    })
  }
    

  prepararEditar (m, indice) {
    this.nuevoSelEditar.moduloID = m.moduloID
    this.nuevoSelEditar.codigo = m.codigo.trim()
    this.nuevoSelEditar.nombre = m.nombre.trim()
    this.nuevoSelEditar.descripcion = m.descripcion.trim()
    this.nuevoSelEditar.icono = m.icono.trim()
    this.nuevoSelEditar.estado = m.estado.trim()
    this.nuevoSelEditar.posicion = indice
  }

  editarModulo () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('moduloSistema',1,'actualizarModulo')
    request.setData(this.nuevoSelEditar)
    this.moduloServicio.actualizar("configuracionInicial", request.claves).subscribe(response => {
      this.modulos[this.nuevoSelEditar.posicion] = this.nuevoSelEditar;
      this.nuevoSelEditar = {
        codigo: '',
        nombre: '',
        descripcion: '',
        icono: '',
        estado: '',
        moduloID: 0,
        posicion: 0
      }
      this.toastr.successToastr(Object(response).responseMsg, 'Confirmacion')
    }, error => {
      console.log('Este es el error: ', error)
    })


    
  }
}
