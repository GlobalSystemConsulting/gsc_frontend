import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuloSistemaComponent } from './modulo-sistema.component';

describe('ModuloSistemaComponent', () => {
  let component: ModuloSistemaComponent;
  let fixture: ComponentFixture<ModuloSistemaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuloSistemaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuloSistemaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
