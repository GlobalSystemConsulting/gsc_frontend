import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuncionSistemaComponent } from './funcion-sistema.component';

describe('FuncionSistemaComponent', () => {
  let component: FuncionSistemaComponent;
  let fixture: ComponentFixture<FuncionSistemaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuncionSistemaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuncionSistemaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
