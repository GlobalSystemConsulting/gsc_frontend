import { Component, OnInit } from '@angular/core';
import {ModuloService} from '../../../servicios/servicio'
import {RequestService} from '../../../servicios/request'
import { ToastrManager } from 'ng6-toastr-notifications'
@Component({
  selector: 'app-funcion-sistema',
  templateUrl: './funcion-sistema.component.html',
  styleUrls: ['./funcion-sistema.component.css']
})
export class FuncionSistemaComponent implements OnInit {

  funciones: any
  subModulos: any
  nuevaFuncion: any = {
    nombre: String,
    descripcion: String,
    url: String,
    clave: String,
    controlador: String,
    interfaz: String,
    icono: String,
    estado:String,
    subModuloID: Number
  }

  nuevaFuncionEditar: any = {
    nombre: String,
    descripcion: String,
    url: String,
    clave: String,
    controlador: String,
    interfaz: String,
    icono: String,
    estado:String,
    subModuloID: Number,
    funcionID: Number,
    posicion: Number
  }

  funcionSel: any


  funcionSeleccionado: any = {
    funcionID: String,
    nombre: String,
    posicion: Number
  }
  

  constructor(private moduloServicio: ModuloService, private requestService: RequestService,
    public toastr: ToastrManager ) {

    this.funciones = []
    this.subModulos = []

    this.nuevaFuncion.nombre = ''
    this.nuevaFuncion.descripcion = ''
    this.nuevaFuncion.url = ''
    this.nuevaFuncion.clave = ''
    this.nuevaFuncion.controlador = ''
    this.nuevaFuncion.interfaz = ''
    this.nuevaFuncion.icono = ''
    this.nuevaFuncion.estado = 'A'
    this.nuevaFuncion.subModuloID = 0

    this.nuevaFuncionEditar.nombre = ''
    this.nuevaFuncionEditar.descripcion = ''
    this.nuevaFuncionEditar.url = ''
    this.nuevaFuncionEditar.clave = ''
    this.nuevaFuncionEditar.controlador = ''
    this.nuevaFuncionEditar.interfaz = ''
    this.nuevaFuncionEditar.icono = ''
    this.nuevaFuncionEditar.estado = 'A'
    this.nuevaFuncionEditar.subModuloID = 0
    this.nuevaFuncionEditar.funcionID = 0
    this.nuevaFuncionEditar.posicion = 0
    
    
    
    this.funcionSel = []

    this.funcionSeleccionado.funcionID = ''
    this.funcionSeleccionado.nombre = ''
    this.funcionSeleccionado.posicion = 0

  }

  ngOnInit() {
    this.listarFunciones()
    this.listarSubModulos()
  }


  listarFunciones() {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('funcionSistema',1,'listarFunciones')
    request.setData({})
    
    this.moduloServicio.listar('configuracionInicial', request.claves).subscribe(response => {
      console.log('Esta es la respuesta: ', Object(response).data)
      this.funciones = Object(response).data
    }, error => {
      console.log('Este es el error: ', error)
    })
  }
  

  listarSubModulos() {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('subModuloSistema',1,'listarSubModulos')
    request.setData({})
    this.moduloServicio.listar('configuracionInicial', request.claves).subscribe(response => {
      this.subModulos = Object(response).data
    }, error => {
      console.log('Este es el error: ', error)
    })
  } 


  eliminarFuncion (idDato, nombre, posicion) {
    this.funcionSeleccionado.funcionID = idDato
    this.funcionSeleccionado.nombre = nombre
    this.funcionSeleccionado.posicion = posicion
    console.log(this.funcionSeleccionado)
  }

  confirmacionElimanarFuncion () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('funcionSistema',1,'eliminarFuncion')
    request.setData({funcionID:this.funcionSeleccionado.funcionID})
    this.moduloServicio.eliminar("configuracionInicial", request.claves).subscribe(response => {
      this.funciones.splice(this.funcionSeleccionado.posicion,1);
    }, error => {
      console.log('Este es el error: ', error)
    })
  }
  
  agregarFuncion () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('funcionSistema',1,'insertarFuncion')
    request.setData(this.nuevaFuncion)
    
    this.moduloServicio.insertar("configuracionInicial", request.claves).subscribe(response => {
      this.funciones.push(this.nuevaFuncion)
      this.nuevaFuncion = {
        nombre: '',
        descripcion: '',
        url: '',
        clave: '',
        controlador: '',
        interfaz: '',
        icono: '',
        estado :'A',
        subModuloID: 0
      }
      this.toastr.successToastr(Object(response).responseMsg, 'Confirmacion')
    }, error => {
      console.log('Este es el error: ', error)
    })
  }


  prepararEditar (m, indice) {
    this.nuevaFuncionEditar.nombre = m.nombre
    this.nuevaFuncionEditar.descripcion = m.descripcion
    this.nuevaFuncionEditar.url = m.url
    this.nuevaFuncionEditar.clave = m.clave
    this.nuevaFuncionEditar.controlador = m.controlador
    this.nuevaFuncionEditar.interfaz = m.interfaz
    this.nuevaFuncionEditar.icono = m.icono
    this.nuevaFuncionEditar.estado = m.estado
    this.nuevaFuncionEditar.subModuloID =  m.subModuloID
    this.nuevaFuncionEditar.funcionID = m.funcionID
    this.nuevaFuncionEditar.posicion = indice
  }

  editarFuncion () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('funcionSistema',1,'actualizarFuncion')
    request.setData(this.nuevaFuncionEditar)

    this.moduloServicio.actualizar("configuracionInicial", request.claves).subscribe(response => {
      this.funciones[this.nuevaFuncionEditar.posicion] = this.nuevaFuncionEditar;
      this.nuevaFuncionEditar = {
        nombre:  '',
        descripcion:  '',
        url:  '',
        clave:  '',
        controlador:  '',
        interfaz:  '',
        icono:  '',
        estado: '',
        subModuloID: 0,
        funcionID: 0,
        posicion: 0
      }
      this.toastr.successToastr(Object(response).responseMsg, 'Confirmacion')
    }, error => {
      console.log('Este es el error: ', error)
    })
  }


    


}
