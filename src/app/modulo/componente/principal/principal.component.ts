import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    // Primera verificacion
    if(!localStorage.getItem('jwt') || !localStorage.getItem('modulos')){
      localStorage.clear();
      location.replace('login');
    }


  }

}
