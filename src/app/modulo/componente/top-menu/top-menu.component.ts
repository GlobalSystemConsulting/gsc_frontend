import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.css']
})
export class TopMenuComponent implements OnInit {


  usuMaster: any = {
    nombre: String,
    rol: String,
    organizacion: String
  }


  constructor(private router: Router) { }

  ngOnInit() {

    this.usuMaster.nombre = localStorage.getItem('nombre')
    this.usuMaster.rol = localStorage.getItem('rol')
    this.usuMaster.organizacion = JSON.parse(localStorage.getItem('organizacion'))

  }


  cerrarSession () {
    localStorage.clear()
    this.router.navigate(['login/']);
  }



}
