import { Component, OnInit } from '@angular/core';
import {ModuloService} from '../../../servicios/servicio'
import {RequestService} from '../../../servicios/request'
import { ToastrManager } from 'ng6-toastr-notifications'

@Component({
  selector: 'app-sub-modulo-sistema',
  templateUrl: './sub-modulo-sistema.component.html',
  styleUrls: ['./sub-modulo-sistema.component.css']
})
export class SubModuloSistemaComponent implements OnInit {

  funciones: any

  modulos: any

  nuevoSubModulo: any = {
    codigo: String,
    nombre: String,
    descripcion: String,
    icono: String,
    estado: String,
    moduloID: Number

  }    
  subModuloSel: any = {
    codigo: String,
    nombre: String,
    descripcion: String,
    icono: String,
    estado: String,
    posicion: Number,
    subModuloID: Number,
    moduloID: Number
  }

  funcionesSel = [];
  //estados = [{id:'A',title:"activo"},{id:'E',title:"eliminado"}];
    
  paramsSubModulo = {count: 10};
  settingSubModulo = { counts: []};

  subModuloSeleccionado: any = {
    funcionID: String,
    nombre: String,
    posicion: Number
  }

  constructor(private moduloServicio: ModuloService, private requestService: RequestService,
    public toastr: ToastrManager ) { 
      this.funciones=[]
      this.modulos=[]

      this.nuevoSubModulo.codigo = ""
      this.nuevoSubModulo.nombre = ""
      this.nuevoSubModulo.descripcion = ""
      this.nuevoSubModulo.icono = ""
      this.nuevoSubModulo.estado = 'A'
      this.nuevoSubModulo.moduloID = 0


      this.subModuloSel.codigo = ""
      this.subModuloSel.nombre = ""
      this.subModuloSel.descripcion = ""
      this.subModuloSel.icono = ""
      this.subModuloSel.estado = ''
      this.subModuloSel.posicion = 0
      this.subModuloSel.subModuloID = 0
      this.subModuloSel.moduloID = 0
  
      this.subModuloSeleccionado.idMod = ''
      this.subModuloSeleccionado.nombre = ''
      this.subModuloSeleccionado.posicion = 0
    }

  ngOnInit() {
    this.listarSubModulos()
    this.listarModulos()
   
  }

  listarSubModulos() {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('subModuloSistema',1,'listarSubModulos')
    request.setData({})
    this.moduloServicio.listar('configuracionInicial', request.claves).subscribe(response => {
      console.log('Esta es la respuesta: ', Object(response).data)
      this.funciones = Object(response).data
    }, error => {
      console.log('Este es el error: ', error)
    })
  }

  eliminarSubModulo (idDato, nombre, posicion) {
    this.subModuloSeleccionado.idMod = idDato
    this.subModuloSeleccionado.nombre = nombre
    this.subModuloSeleccionado.posicion = posicion
    console.log(this.subModuloSeleccionado)

  }

  confirmacionElimanarFuncion () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('subModuloSistema',1,'eliminarSubModulo')
    request.setData({subModuloID:this.subModuloSeleccionado.idMod})
    this.moduloServicio.eliminar("configuracionInicial", request.claves).subscribe(response => {
      this.funciones.splice(this.subModuloSeleccionado.posicion,1);
    }, error => {
      console.log('Este es el error: ', error)
    })
  }

  agregarSubModulo () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('subModuloSistema',1,'insertarSubModulo')
    request.setData(this.nuevoSubModulo)
    
    this.moduloServicio.insertar("configuracionInicial", request.claves).subscribe(response => {
      this.funciones.push(this.nuevoSubModulo)
      this.nuevoSubModulo = {
        codigo: '',
        nombre: '',
        descripcion: '',
        icono: '',
        estado: 'A'
      }
      this.toastr.successToastr(Object(response).responseMsg, 'Confirmacion')
    }, error => {
      console.log('Este es el error: ', error)
    })
  }
  prepararEditar (m, indice) {
    this.subModuloSel.subModuloID = m.subModuloID
    this.subModuloSel.moduloID = m.moduloID
    this.subModuloSel.codigo = m.codigo.trim()
    this.subModuloSel.nombre = m.nombre.trim()
    this.subModuloSel.descripcion = m.descripcion.trim()
    this.subModuloSel.icono = m.icono.trim()
    this.subModuloSel.estado = m.estado.trim()
    this.subModuloSel.posicion = indice
    console.log(this.subModuloSel)
    
  }

  editarSubModulo () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('subModuloSistema',1,'actualizarSubModulo')
    request.setData(this.subModuloSel)
    this.moduloServicio.actualizar("configuracionInicial", request.claves).subscribe(response => {
      this.funciones[this.subModuloSel.posicion] = this.subModuloSel;
      this.subModuloSel = {
        codigo: '',
        nombre: '',
        descripcion: '',
        icono: '',
        estado: '',
        subModuloID: 0,
        posicion: 0
      }
      this.toastr.successToastr(Object(response).responseMsg, 'Confirmacion')
    }, error => {
      console.log('Este es el error: ', error)
    })
  }

  listarModulos(){
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('moduloSistema',1,'listarModulos')
    request.setData({})
    this.moduloServicio.listar('configuracionInicial', request.claves).subscribe(response => {
      this.modulos = Object(response).data
    }, error => {
      console.log('Este es el error: ', error)
    })
  }

}
