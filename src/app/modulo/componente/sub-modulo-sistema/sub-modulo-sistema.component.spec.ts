import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubModuloSistemaComponent } from './sub-modulo-sistema.component';

describe('SubModuloSistemaComponent', () => {
  let component: SubModuloSistemaComponent;
  let fixture: ComponentFixture<SubModuloSistemaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubModuloSistemaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubModuloSistemaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
