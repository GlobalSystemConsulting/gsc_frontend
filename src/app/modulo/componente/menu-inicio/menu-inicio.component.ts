import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router' 

@Component({
  selector: 'app-menu-inicio',
  templateUrl: './menu-inicio.component.html',
  styleUrls: ['./menu-inicio.component.css']
})
export class MenuInicioComponent implements OnInit {
  menuParent = [];
  subModuloActual = [];
  vistaActual = [];
  menu = [];
  modNom = "";
  subModNom = "";
  visNom = "";
  movil = true;

  funcionalidades = [];
  usuMaster:any;
  menuPrincipal:any;


  constructor(private router: Router) {
    this.usuMaster = {usuario:"",rol:"",organizacion:""};
    this.menuPrincipal = [];
   }

   elegirVista(vista) {
    var rutaNueva = 'principal/'+vista.clave;
    this.router.navigate([rutaNueva]);
  }
    

  
  ngOnInit() {
      //configuracion inicial "no tocar" 
      this.usuMaster.usuario = JSON.parse( window.atob( localStorage.getItem('usuario')) );      
      this.usuMaster.rol = JSON.parse( window.atob(localStorage.getItem('rol')) );
      this.usuMaster.organizacion = JSON.parse( window.atob(localStorage.getItem('organizacion')) );
      
      if(localStorage.getItem('area'))
        this.usuMaster.area = JSON.parse( window.atob(localStorage.getItem('area')) );
        
    
      if(localStorage.getItem('area'))
          this.usuMaster.area = JSON.parse( window.atob(localStorage.getItem('area')) );
      
      this.menuPrincipal = JSON.parse( localStorage.getItem('modulos') );
      
      //sacando funcionalidades  //////////////////////////////////////////////////////////
      for(var i=0;i<this.menuPrincipal.length ;i++ ){
          var Modulost = this.menuPrincipal[i].subModulos;
          for(var j=0;j < Modulost.length; j++ ){                
              var funcionest = Modulost[j].funciones; 
              for(var k=0;k < funcionest.length;k++ ){ 
                  this.funcionalidades.push(funcionest[k]);                    
              }
          }  
      }
      console.log("Funcionalidades sacadas",this.funcionalidades)
  }

  inicio(){
    this.menuParent = [];
    this.subModuloActual = [];
    this.vistaActual = [];
    this.menu = [];
    this.modNom = "";
    this.subModNom = "";
    this.visNom = "";
    //location.path("menuInicio");
};        

showLoading () {
   // this.showLoadingIcon = true;
};


}
