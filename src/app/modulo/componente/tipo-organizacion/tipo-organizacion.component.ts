import { Component, OnInit } from '@angular/core';
import { ModuloService } from '../../../servicios/servicio'
import { RequestService } from '../../../servicios/request'
import { ToastrManager } from 'ng6-toastr-notifications'

@Component({
  selector: 'app-tipo-organizacion',
  templateUrl: './tipo-organizacion.component.html',
  styleUrls: ['./tipo-organizacion.component.css']
})
export class TipoOrganizacionComponent implements OnInit {

  tipoOrganizaciones :any
  
  roles :any
  rolesSel :any
  
  rolesEdi : any;
  rolesSelEdi : any;
  
  nuevoTipoOrganizacion: any = {
    codigo: String,
    nombre: String,
    descripcion: String,
    estado: String,
    roles: []
  }
  tipoOrganizacionSel: any = {
    codigo: String,
    nombre: String,
    descripcion: String,
    estado: String,
    roles: [],
    tipoOrganizacionID: Number,
    posicion: Number
  }

  tipoOrganizacionSeleccionado: any = {
    tipoOrganizacionID: String,
    nombre: String,
    posicion: Number
  }

  constructor(private moduloServicio: ModuloService, private requestService: RequestService,
    public toastr: ToastrManager) {

      this.tipoOrganizaciones = []
      this.roles= []
      this.rolesSel = []
      this.rolesEdi = []
      this.rolesSelEdi = []

      this.nuevoTipoOrganizacion.codigo = ''
      this.nuevoTipoOrganizacion.nombre = ''
      this.nuevoTipoOrganizacion.descripcion = ''
      this.nuevoTipoOrganizacion.estado = 'A'
      this.nuevoTipoOrganizacion.roles = []

      this.tipoOrganizacionSel.codigo = ''
      this.tipoOrganizacionSel.nombre = ''
      this.tipoOrganizacionSel.descripcion = ''
      this.tipoOrganizacionSel.estado = 'A'
      this.tipoOrganizacionSel.roles = []
      this.tipoOrganizacionSel.tipoOrganizacionID = 0
      this.tipoOrganizacionSel.posicion = 0

      this.tipoOrganizacionSeleccionado.tipoOrganizacionID = ''
      this.tipoOrganizacionSeleccionado.nombre = ''
      this.tipoOrganizacionSeleccionado.posicion = 0
    }
    
    ngOnInit() {
      this.listarTipoOrganizaciones()
      this.listarRoles()
    }

listarTipoOrganizaciones(){
      var request = new RequestService()
      request.setIdentity("admin")
      request.setScope('web')
      request.setCmd('tipoOrganizacion', 1, 'listarTipoOrganizaciones')
      request.setData({})
      this.moduloServicio.listar("configuracionInicial",request.claves).subscribe(response =>{
            this.tipoOrganizaciones=Object(response).data
        },error =>{
            console.log('Este es el error',error);
        });
    }

  agregarTipoOrganizacion() {
    
    for(var i=0;i<this.rolesSel.length;i++ )
      this.nuevoTipoOrganizacion.roles.push( this.rolesSel[i] )

    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('tipoOrganizacion', 1, 'insertarTipoOrganizacion')
    request.setData(this.nuevoTipoOrganizacion)
    this.moduloServicio.insertar('configuracionInicial', request.claves).subscribe(response => {     
    this.tipoOrganizaciones.push(this.nuevoTipoOrganizacion)
    this.nuevoTipoOrganizacion = {
        codigo: '',
        nombre: '',
        descripcion: '',
        estado: 'A',
        roles: []
      }
      this.toastr.successToastr(Object(response).responseMsg, 'Confirmacion')
    }, error => {
      console.log('Este es el error: ', error)
    })
    console.log(this.nuevoTipoOrganizacion)
  }

listarRoles(){

    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('rol', 1, 'listarRolesConFunciones')
    request.setData({})

    this.moduloServicio.listar("configuracionInicial",request.claves).subscribe(response =>{
          this.roles=Object(response).data
      },error =>{
          console.log('Este es el error',error);
      });
  }

seleccionarRol(i){
    this.rolesSel.push( this.roles[i] );
    this.roles[i].ver = true;
}

desSeleccionarRol (i){
  this.rolesSel[i].ver = false;
  this.rolesSel.splice(i,1);
}

seleccionarRol2 (i){
  this.rolesSelEdi.push( this.rolesEdi[i] );
  this.rolesEdi[i].ver = true;
};

prepararEditar (m, indice) {
  
  this.tipoOrganizacionSel.codigo = m.codigo.trim()
  this.tipoOrganizacionSel.nombre = m.nombre.trim()
  this.tipoOrganizacionSel.descripcion = m.descripcion.trim()
  this.tipoOrganizacionSel.estado = m.estado
  this.tipoOrganizacionSel.tipoOrganizacionID = m.tipoOrganizacionID
  this.tipoOrganizacionSel.posicion = indice

  console.log(m)

}

editarTipoOrganizacion () {
  var request = new RequestService()
  request.setIdentity("admin")
  request.setScope('web')
  request.setCmd('tipoOrganizacion',1,'actualizarTipoOrganizacion')
  request.setData(this.tipoOrganizacionSel)
  this.moduloServicio.actualizar("configuracionInicial", request.claves).subscribe(response => {
    this.tipoOrganizaciones[this.tipoOrganizacionSel.posicion] = this.tipoOrganizacionSel;
    this.tipoOrganizacionSel = {
      tipoOrganizacionID: 0,
      codigo: '',
      nombre: '',
      descripcion: '',
      estado: '',
      roles: [],
      posicion: 0
    }
    this.toastr.successToastr(Object(response).responseMsg, 'Confirmacion')
  }, error => {
    console.log('Este es el error: ', error)
  })
}

eliminarTipoOrganizacion (idDato, nombre, posicion) {
  this.tipoOrganizacionSeleccionado.tipoOrganizacionID = idDato
  this.tipoOrganizacionSeleccionado.nombre = nombre
  this.tipoOrganizacionSeleccionado.posicion = posicion
}

confirmacionElimanarModulo () {
  var request = new RequestService()
  request.setIdentity("admin")
  request.setScope('web')
  request.setCmd('tipoOrganizacion',1,'eliminarTipoOrganizacion')
  request.setData({tipoOrganizacionID:this.tipoOrganizacionSeleccionado.tipoOrganizacionID})
  this.moduloServicio.eliminar("configuracionInicial", request.claves).subscribe(response => {
    this.tipoOrganizaciones.splice(this.tipoOrganizacionSeleccionado.posicion,1);
  }, error => {
    console.log('Este es el error: ', error)
  })
}
}
