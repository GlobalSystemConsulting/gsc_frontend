import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertaSistemaComponent } from './alerta-sistema.component';

describe('AlertaSistemaComponent', () => {
  let component: AlertaSistemaComponent;
  let fixture: ComponentFixture<AlertaSistemaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertaSistemaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertaSistemaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
