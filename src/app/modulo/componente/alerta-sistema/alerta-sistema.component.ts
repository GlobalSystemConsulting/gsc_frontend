import { Component, OnInit } from '@angular/core';
import { ModuloService } from '../../../servicios/servicio'
import { RequestService } from '../../../servicios/request'
import { ToastrManager } from 'ng6-toastr-notifications'

@Component({
  selector: 'app-alerta-sistema',
  templateUrl: './alerta-sistema.component.html',
  styleUrls: ['./alerta-sistema.component.css']
})
export class AlertaSistemaComponent implements OnInit {

  alertas : any;
    
  paramsAlerta = {count: 10};
  funciones : any;
 
  nuevaAlerta: any = {
    nombre: String,
    descripcion: String,
    accion: String,
    tipo: String,
    alertaID: Number,
    funcionID: Number
  }

  nuevaAlertaEditar: any = {
    nombre: String,
    descripcion: String,
    accion: String,
    tipo: String,
    alertaID: Number,
    funcionID: Number
  }

  alertaSeleccionada: any = {
    funcionID: String,
    nombre: String,
    posicion: Number
  }
  constructor(private moduloServicio: ModuloService, private requestService: RequestService,
    public toastr: ToastrManager) {
      this.funciones=[]
      this.alertas =[]

    this.nuevaAlerta.nombre = ''
    this.nuevaAlerta.descripcion = ''
    this.nuevaAlerta.accion = ''
    this.nuevaAlerta.tipo = 'A'
    this.nuevaAlerta.alertaID = 0
    this.nuevaAlerta.funcionID = 0

    this.nuevaAlertaEditar.nombre = ''
    this.nuevaAlertaEditar.descripcion = ''
    this.nuevaAlertaEditar.accion = ''
    this.nuevaAlertaEditar.tipo = 'A'
    this.nuevaAlertaEditar.alertaID = 0
    this.nuevaAlertaEditar.funcionID = 0
    this.nuevaAlertaEditar.posicion = 0
    

    this.alertaSeleccionada.funcionID = ''
    this.alertaSeleccionada.nombre = ''
    this.alertaSeleccionada.posicion = 0
    }

  ngOnInit() {
    this.listarAlertas()
    this.listarFunciones()
  }

  listarAlertas(){
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('alertaSistema', 1, 'listarAlertas')
    request.setData({})
    this.moduloServicio.listar("configuracionInicial",request.claves).subscribe(response =>{
      console.log('Esta es la respuesta: ', Object(response).data)    
      this.alertas=Object(response).data
      },error =>{
          console.log('Este es el error',error);
      });
      console.log(this.alertas)
  }


  eliminarAlerta (idDato, nombre, posicion) {
    this.alertaSeleccionada.alertaID = idDato
    this.alertaSeleccionada.nombre = nombre
    this.alertaSeleccionada.posicion = posicion
    console.log(this.alertaSeleccionada)
  }

  confirmacionElimanarAlerta() {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('alertaSistema',1,'eliminarAlerta')
    request.setData({alertaID:this.alertaSeleccionada.alertaID})
    this.moduloServicio.eliminar("configuracionInicial", request.claves).subscribe(response => {
      this.alertas.splice(this.alertaSeleccionada.posicion,1);
    }, error => {
      console.log('Este es el error: ', error)
    })
  }
  
  agregarAlerta () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('alertaSistema',1,'insertarAlerta')
    request.setData(this.nuevaAlerta)
    this.moduloServicio.insertar("configuracionInicial", request.claves).subscribe(response => {
      this.alertas.push(this.nuevaAlerta)
      this.nuevaAlerta = {
       nombre : '',
       descripcion : '',
       accion : '',
       tipo : 'A',
       alertaID : 0,
       funcionID : 0
      }
      this.toastr.successToastr(Object(response).responseMsg, 'Confirmacion')
    }, error => {
      console.log('Este es el error: ', error)
    })

  }


  prepararEditar (m, indice) {
    this.nuevaAlertaEditar.nombre = m.nombre
    this.nuevaAlertaEditar.descripcion = m.descripcion
    this.nuevaAlertaEditar.accion = m.accion
    this.nuevaAlertaEditar.tipo = m.tipo
    this.nuevaAlertaEditar.alertaID = m.alertaID
    this.nuevaAlertaEditar.funcionID = m.funcionID
    this.nuevaAlertaEditar.posicion = indice
  }

  editarAlerta () {
    var request = new RequestService()
    request.setIdentity("admin")
    request.setScope('web')
    request.setCmd('alertaSistema',1,'actualizarAlerta')
    request.setData(this.nuevaAlertaEditar)

    this.moduloServicio.actualizar("configuracionInicial", request.claves).subscribe(response => {
      this.alertas[this.nuevaAlertaEditar.posicion] = this.nuevaAlertaEditar;
      this.nuevaAlertaEditar = {
        nombre : '',
        descripcion : '',
        accion : '',
        tipo : 'A',
        alertaID : 0,
        funcionID : 0,
        posicion: 0
      }
      this.toastr.successToastr(Object(response).responseMsg, 'Confirmacion')
    }, error => {
      console.log('Este es el error: ', error)
    })
  }

  listarFunciones(){
        var request = new RequestService()
        request.setIdentity("admin")
        request.setScope('web')
        request.setCmd('funcionSistema', 1, 'listarFunciones')
        request.setData({})
        this.moduloServicio.listar("configuracionInicial",request.claves).subscribe(response =>{
        this.funciones=Object(response).data
          },error =>{
              console.log('Este es el error',error);
          });
    }; 
    
}
