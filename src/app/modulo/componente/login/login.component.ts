import { Component, OnInit } from '@angular/core'
import { ModuloService } from '../../../servicios/servicio'
import { RequestService } from '../../../servicios/request'
import { ToastrManager } from 'ng6-toastr-notifications'
import { Router } from '@angular/router'
declare function loginInit(): any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],

})

export class LoginComponent implements OnInit {
  

  usuario: any = {
    nombre: String,
    password: String,
    organizacionID: String,
    rolID: String
  }

  colores: any
  bloquear: boolean
  session: any
  organizaciones: any

  login1: boolean
  login2: boolean

  ayudaSeleccionOrga: any

  funciones: any 

  constructor(private moduloServicio: ModuloService, private requestService: RequestService,
    public toastr: ToastrManager, private router: Router) {
    this.usuario.nombre = ''
    this.usuario.password = ''
    this.usuario.organizacionID = ''
    this.usuario.rolID = ''

    this.colores = []
    this.bloquear = false
    
    this.session = {
      mensaje: '',
      rodID: '',
      organizacion: {},
      organizacionesLista: [],
      indice: ''
    }
    this.organizaciones = []
    this.ayudaSeleccionOrga = {}

    this.funciones = [[], [], [], [], [], []]

    this.login1 = true
    this.login2 = false

  }

  ngOnInit() {
    localStorage.clear()
    this.inicializarColores()
    loginInit();
  }


  inicializarColores () {
   
    this.colores.push("#fa8564")
    this.colores.push("#1fb5ad")
    this.colores.push("#9466b5")
    this.colores.push("#58C9F3")
    this.colores.push("#CE93D8")
    this.colores.push("#90CAF9")
    this.colores.push("#4DB6AC")
    this.colores.push("#1DE9B6")
    this.colores.push("#DCE775")
    this.colores.push("#EEFF41")
    this.colores.push("#FF9800")
    this.colores.push("#BDBDBD")
    this.colores.push("#F48FB1")
    this.colores.push("#3F51B5")
    this.colores.push("#006064")
    this.colores.push("#FF6F00")
    this.colores.push("#8D6E63")
    this.colores.push("#B71C1C")
    this.colores.push("#FFEE58")
    this.colores.push("#607D8B")
    this.colores.push("#FFF176")
    this.colores.push("#43A047")
    this.colores.push("#FF5722")

  }

  iniciarSesionAntiguo() {

    var request = new RequestService()
    request.setIdentity(this.usuario.nombre)
    request.setScope('web')
    request.setCmd('login', 1, 'signin')
    request.setData(this.usuario)


    this.moduloServicio.login('login', request.claves).subscribe(response => {
      var objResponse = Object(response).data
      if (!objResponse) {
        this.toastr.errorToastr('El usuario no existe, usuario o contraseÃ±a incorrectos', 'Error!')
      } else {
        localStorage.setItem('jwt', objResponse.jwt)
        localStorage.setItem('nombre', objResponse.nombre)
        localStorage.setItem('rol', objResponse.rol)
        localStorage.setItem('organizacion', JSON.stringify(objResponse.organizacion))
        localStorage.setItem('modulos', JSON.stringify(objResponse.modulos))
        //location.replace( urls.BASECONTEXTO + objResponse.url+"#menuInicio" )
        this.toastr.successToastr('Bienvenido', 'Exito!')
        this.router.navigate(['principal/moduloSistema/'])
      }
    }, error => {
      this.toastr.errorToastr('Hubo un error.', 'Error!')
      console.log('Esre es el error: ', error)
    })
  }


  iniciarSesion() {
    if (!this.usuario.password || this.usuario.password == "") {
      this.toastr.errorToastr('Ingrese su password', 'MENSAJE!')
      return
    }
    if (this.session.organizacion || this.session.organizacion == null) {
      this.toastr.errorToastr('Seleccine una Organizacion', 'MENSAJE!')
      return
    }
    if (!this.session.rolID || this.session.rolID == null) {
      this.toastr.errorToastr('Seleccine un Rol', 'MENSAJE!')
      return
    }


    this.usuario.organizacionID =  this.organizaciones[Number(this.session.indice)].organizacionID
    this.usuario.rolID =  this.session.rolID
  
    console.log('Este es el usuario: ',this.usuario )

    var request = new RequestService()
    request.setIdentity(this.usuario.nombre)
    request.setScope('web')
    request.setCmd('login', 1, 'signin')
    request.setData(this.usuario)
    this.bloquear = true

    this.moduloServicio.login('login', request.claves).subscribe(response => {

      var objResponse = Object(response).data
      if (!objResponse) {
        this.toastr.errorToastr('El usuario no existe, usuario o contraseÃ±a incorrectos', 'Error!')
      } else {

        console.log('Esta es la respuesta del login: ', objResponse)

        localStorage.setItem('jwt', objResponse.jwt)
        localStorage.setItem('usuario', window.btoa(JSON.stringify(objResponse.usuario)))
        localStorage.setItem('rol', window.btoa(JSON.stringify(objResponse.rol)))
        localStorage.setItem('organizacion', window.btoa(JSON.stringify(objResponse.organizacion)))

        if (objResponse.personalizacion)
          localStorage.setItem('personalizacion', window.btoa(JSON.stringify(objResponse.personalizacion)))
        if (objResponse.area)
          localStorage.setItem('area', window.btoa(JSON.stringify(objResponse.area)))


        for (let item of objResponse.modulos) {
          item.color = this.colores[item.moduloID - 1]
          for (let sub of  item.subModulos ) {
            sub.color = this.colores[sub.subModuloID - 1]
            for (let fun of sub.funciones) {
              fun.color = this.colores[sub.subModuloID - 1]
              this.funciones[fun.tipo].push(fun)
            }
          }

        }
        
        localStorage.setItem('modulos', JSON.stringify(objResponse.modulos))
        localStorage.setItem('funciones', JSON.stringify(this.funciones))
        localStorage.setItem('events', JSON.stringify(objResponse.events))
        this.router.navigate(['principal/menu-inicio'])
        //location.replace(urls.BASECONTEXTO + objResponse.url + "#menuInicio")
        //location.replace( urls.BASECONTEXTO + objResponse.url+"#menuInicio" )
        this.toastr.successToastr('Bienvenido', 'Exito!')

      }
      



    }, error => {
      this.toastr.errorToastr('Hubo un error.', 'Error!')
      console.log('Esre es el error: ', error)
    })

    

  }

  identificarUsuario() {
    if (!this.usuario.nombre || this.usuario.nombre == "") {
      this.toastr.errorToastr('Ingrese nombre de usuario', 'MENSAJE!')
      return
    }

    var request = new RequestService()
    request.setIdentity(this.usuario.nombre)
    request.setScope('web')
    request.setCmd('login', 1, 'search')
    request.setData(this.usuario)
    this.bloquear = true


    this.moduloServicio.login('login', request.claves).subscribe(response => {
      var objResponse = Object(response).data
      if (objResponse) {
        
        var indice = 0
        for (let a of objResponse) {
          a.indice = indice
          this.organizaciones.push(a)
          indice++
        }
        
        this.session.organizacion = this.organizaciones[0].indice
        this.session.indice = this.organizaciones[0].indice
        this.session.rolID = this.organizaciones[0].roles[0].rolID
        this.session.organizacionesLista = this.organizaciones[0].roles
        this.bloquear = false
        this.login1 = false
        this.login2 = true
              
      }
      this.bloquear = false
    }, error => {
      this.toastr.errorToastr('Hubo un error.', 'Error!')
      console.log('Esre es el error: ', error)
    })
  }

  regresar() {
    this.login1 = true
    this.login2 =false
    this.session = {
      mensaje: '',
      rodID: '',
      organizacion: {},
      organizacionesLista: [],
      indice: ''
    }
    this.organizaciones = []
    this.ayudaSeleccionOrga = {}
    this.usuario = {
      nombre: '',
      password: '',
      organizacionID: '',
      rolID: ''
    }
  }

  seleccionarOrg (org) {
    this.session.organizacionesLista = this.organizaciones[org].roles
    this.session.rolID = this.organizaciones[org].roles[0].rolID 
  }

}
