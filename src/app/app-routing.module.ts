import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { PrincipalComponent } from './modulo/componente/principal/principal.component';
import { MainNavComponent } from './main-nav/main-nav.component';
const routes: Routes = [
  {
    path: 'login',
    loadChildren: './modulo/ruta/login/login.module#LoginModule'
  },
  {
    path: 'principal',
    component: MainNavComponent,
    children: [
      {
        path: 'menu-inicio',
        loadChildren: './modulo/ruta/menu-inicio/menu-inicio.module#MenuInicioModule'
      },
      {
        path: '',
        redirectTo: 'moduloSistema',
        pathMatch: 'full'
      },
      {
        path: 'moduloSistema',
        loadChildren: './modulo/ruta/modulo-sistema/modulo-sistema.module#ModuloSistemaModule'
      },
      {
        path: 'funcionSistema',
        loadChildren: './modulo/ruta/funcion-sistema/funcion-sistema.module#FuncionSistemaModule'
      },
      {
        path: 'organizacion',
        loadChildren: './modulo/ruta/agregados/configuracion-inicial/organizacion/organizacion.module#OrganizacionModule'
      },
      {
        path: 'usuarioSistema',
        loadChildren: './modulo/ruta/usuario/usuario.module#UsuarioModule'
      },
      {
        path: 'guiaEmpresas',
        loadChildren: './modulo/ruta/guia-empresas/guia-empresas.module#GuiaEmpresasModule'
      },
      {
        path: 'subModuloSistema',
        loadChildren: './modulo/ruta/sub-modulo-sistema/sub-modulo-sistema.module#SubModuloSistemaModule'
      },
      {
        path: 'alertaSistema',
        loadChildren: './modulo/ruta/alerta-sistema/alerta-sistema.module#AlertaSistemaModule'
      }
      ,
      {
        path: 'tipoOrganizacion',
        loadChildren: './modulo/ruta/tipo-organizacion/tipo-organizacion.module#TipoOrganizacionModule'
      }   ,
      {
        path: 'tipoArea',
        loadChildren: './modulo/ruta/tipo-area/tipo-area.module#TipoAreaModule'
      }   ,
      {
        path: 'rol',
        loadChildren: './modulo/ruta/roles/roles.module#RolesModule'
      }
    ]
  },
  
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), FormsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
