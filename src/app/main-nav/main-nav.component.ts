import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router' 

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent {
  
  menu: any;
  movil = true;
  vistaActual = [];

  menuAnteriorCompleto: any;
  usuMaster :any;
  menuPrincipal = [];
  funcionalidades = [];
  subModNom = "";
  funciones:any;
  modNom:any;
  visNom:any;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  constructor(private breakpointObserver: BreakpointObserver, private router: Router) {
    this.usuMaster = {usuario:"",rol:"",organizacion:""};
    this.menuPrincipal = [];
  }

  ngOnInit() {
    //configuracion inicial "no tocar" 
    this.usuMaster.usuario = JSON.parse( window.atob( localStorage.getItem('usuario')) );      
    this.usuMaster.rol = JSON.parse( window.atob(localStorage.getItem('rol')) );
    this.usuMaster.organizacion = JSON.parse( window.atob(localStorage.getItem('organizacion')) );
    
    
    this.menuPrincipal = JSON.parse( localStorage.getItem('modulos') );
  
          //sacando funcionalidades  //////////////////////////////////////////////////////////
          for(var i=0;i<this.menuPrincipal.length ;i++ ){
            var Modulost = this.menuPrincipal[i].subModulos;
            for(var j=0;j < Modulost.length; j++ ){                
                var funcionest = Modulost[j].funciones; 
                for(var k=0;k < funcionest.length;k++ ){ 
                    this.funcionalidades.push(funcionest[k]);                    
                }
            }  
        }
        console.log("Funcionalidades sacadas",this.funcionalidades)
  
        this.funciones = JSON.parse( localStorage.getItem('funciones') );
        this.menu = JSON.parse( localStorage.getItem('menu'));
        this.modNom = localStorage.getItem('modNom');
        this.subModNom = localStorage.getItem('subModNom');
        this.visNom = JSON.parse( localStorage.getItem('visNom'));
  }
  
    menuToggle (e,subModulo){
      
      for (let s of this.menuAnteriorCompleto) {
        if (s.indice == subModulo.indice) {
          console.log('Entro xa:', subModulo)
          subModulo.subModulos = s.subModulos 
        }
      }
      
      for (let a of this.menu) {
        if (a.indice != subModulo.indice) {
          this.menu[a.indice].subModulos = []
          this.menu[a.indice].subModulos.push({})
        }
      }
      
    }
  
    elegirVista(vista) {
      var rutaNueva = 'principal/'+vista.clave;
      this.router.navigate([rutaNueva]);
    }
  
   /* elegirVista(vista,subModulo){
          
      this.vistaActual = vista;
      this.visNom = vista.nombre;
      localStorage.setItem('visNom', JSON.stringify(this.visNom));
      if(subModulo){
          this.subModNom = this.movil? subModulo.codigo: subModulo.nombre;;
          localStorage.setItem('subModNom', this.subModNom);
      }
     // this.router.navigate(['organizacion']);
      
  };*/
  cerrarSession () {
    localStorage.clear()
    this.router.navigate(['login/']);
  }

  
  }
